/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit, Input, OnChanges, Output } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
// import { BDataModelService } from '../service/bDataModel.service';
import { NDataModelService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { EventEmitter } from '@angular/core';
import { logobject } from 'app/models';
import { weather } from '../../sd-services/weather'

/**
 * Service import Example :
 * import { HeroService } from '../../services/hero/hero.service';
 */

/**
* 
* Service Designer import Example - Service Name - HeroService
* import { HeroService } from 'app/sd-services/HeroService';
*/


@Component({
    selector: 'bh-weathercard',
    templateUrl: './weathercard.template.html'
})

export class weathercardComponent extends NBaseComponent implements OnInit {
    mm: ModelMethods;
    @Output('log') res: EventEmitter<logobject> = new EventEmitter<logobject>();
    weatherdata: any;
    localStorage = localStorage;
    showCard = false;
   constructor(private bdms: NDataModelService,
        public weather: weather) {
        super();
        this.mm = new ModelMethods(bdms);
    }  

    ngOnInit() {
    }

    async getWeatherModelr(cityName) {
        try {
            this.weatherdata = (await this.weather.weatherreport(cityName)).local.response;
            console.log('response', this.weatherdata);
            this.processWeather(cityName);
        } catch (e) {
            console.log('error', e);
            this.res.emit({
                type: 'error',
                message: typeof e.message === 'string' ? e.message : 'Error Occured While Retrieving the Weather Data.'
            });
        }
    }

    async processWeather(cityName) {
        if (typeof this.weatherdata === 'object' && Object.keys(this.weatherdata).length > 0) {
            this.showCard = true;
            localStorage.lastCity = cityName;
            this.res.emit({
                type: 'info',
                message: 'Successfully Retrieved the Weather Data for city: ' + cityName
            });
        } else {
            this.showCard = false;
            this.res.emit({
                type: 'info',
                message: 'Weather Data is Empty for city: ' + cityName
            });
        }
    }
}