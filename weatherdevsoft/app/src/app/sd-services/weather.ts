/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE CLASS NAME*/
import { Injectable } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { SDBaseService } from '../n-services/SDBaseService';
import { environment } from '../../environments/environment';
import {
  NAlertComponent,
  NAlertService,
  NFileIOService,
  NFileUploadComponent
} from 'neutrinos-module';
import {
  NDataModelService,
  NAuthGuardService,
  NHTTPLoaderService,
  NLocalStorageService,
  NLoginService,
  NLogoutService,
  NNotificationService,
  NPubSubService,
  NSessionStorageService,
  NSnackbarService,
  NSystemService,
  NTokenService
} from 'neutrinos-seed-services';
//CORE_REFERENCE_IMPORTS

declare const window: any;
declare const cordova: any;

@Injectable()
export class weather {
  systemService = NSystemService.getInstance();
  appProperties;

  constructor(
    private http: HttpClient,
    private matSnackBar: MatSnackBar,
    private sdService: SDBaseService,
    private sessionStorage: NSessionStorageService,
    private tokenService: NTokenService,
    private router: Router,
    private httpLoaderService: NHTTPLoaderService,
    private dataModelService: NDataModelService,
    private loginService: NLoginService,
    private authGuardService: NAuthGuardService,
    private localStorageService: NLocalStorageService,
    private logoutService: NLogoutService,
    private notificationService: NNotificationService,
    private pubsubService: NPubSubService,
    private snackbarService: NSnackbarService,
    private alertService: NAlertService,
    private fileIOService: NFileIOService
  ) {}

  //   service flows_weather

  public async weatherreport(cityName = undefined, ...others) {
    try {
      let bh = {
        input: { cityName: cityName },
        local: { response: undefined, modelrApiUrl: undefined }
      };
      bh = this.__constructDefault(bh);

      bh = await this.sd_YG34Of52LSz8WvB8(bh);
      //appendnew_next_weatherreport
      //Start formatting output variables
      let outputVariables = {
        input: {},
        local: { response: bh.local.response }
      };
      //End formatting output variables
      return outputVariables;
    } catch (e) {
      throw e;
    }
  }
  //appendnew_flow_weather_Start

  async sd_YG34Of52LSz8WvB8(bh) {
    try {
      if (
        this.sdService.operators['istype'](
          bh.input.cityName,
          'undefined',
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_BRXOLZPp0xIVQUP6(bh);
      }
      if (
        this.sdService.operators['null'](
          bh.input.cityName,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_BRXOLZPp0xIVQUP6(bh);
      }
      if (
        this.sdService.operators['empty'](
          bh.input.cityName,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_BRXOLZPp0xIVQUP6(bh);
      }
      if (
        this.sdService.operators['istype'](
          bh.input.cityName,
          'string',
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_VQmS6DhXGfVqTDx4(bh);
      }

      return bh;
    } catch (e) {
      throw e;
    }
  }

  async sd_BRXOLZPp0xIVQUP6(bh) {
    try {
      console.log(new Date().toLocaleTimeString(), bh);

      //appendnew_next_sd_BRXOLZPp0xIVQUP6
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_VQmS6DhXGfVqTDx4(bh) {
    try {
      bh.local.modelrApiUrl = `http://localhost:24483/api/weather?cityName=${bh.input.cityName}`;

      bh = await this.sd_QKJbQiLmmUev7iwz(bh);
      this.sd_kQFSRV86UtutWlDu(bh);
      //appendnew_next_sd_VQmS6DhXGfVqTDx4
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_QKJbQiLmmUev7iwz(bh) {
    try {
      let requestOptions = {
        url: bh.local.modelrApiUrl,
        method: 'get',
        responseType: 'json',
        reportProgress: undefined,
        headers: {},
        params: {},
        body: undefined
      };
      bh.local.response = await this.sdService.nHttpRequest(requestOptions);
      //appendnew_next_sd_QKJbQiLmmUev7iwz
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_kQFSRV86UtutWlDu(bh) {
    try {
      console.log(new Date().toLocaleTimeString(), bh.local.modelrApiUrl);

      //appendnew_next_sd_kQFSRV86UtutWlDu
      return bh;
    } catch (e) {
      throw e;
    }
  }
  //appendnew_node

  __constructDefault(bh) {
    const system: any = {};

    try {
      system.currentUser = this.sessionStorage.getValue('userObj');
      system.environment = environment;
      system.tokenService = this.tokenService;
      system.deviceService = this.systemService;
      system.router = this.router;
      system.httpLoaderService = this.httpLoaderService;
      system.dataModelService = this.dataModelService;
      system.loginService = this.loginService;
      system.authGuardService = this.authGuardService;
      system.localStorageService = this.localStorageService;
      system.logoutService = this.logoutService;
      system.notificationService = this.notificationService;
      system.pubsubService = this.pubsubService;
      system.snackbarService = this.snackbarService;
      system.alertService = this.alertService;
      system.fileIOService = this.fileIOService;

      Object.defineProperty(bh, 'system', {
        value: system,
        writable: false
      });

      return bh;
    } catch (e) {
      throw e;
    }
  }
}
