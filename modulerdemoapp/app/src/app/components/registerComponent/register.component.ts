/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
// import { BDataModelService } from '../service/bDataModel.service';
import { NDataModelService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { FormGroup } from '@angular/forms';
import { register } from '../../sd-services/register';
/**
 * Service import Example :
 * import { HeroService } from '../../services/hero/hero.service';
 */

/**
* 
* Serivice Designer import Example - Service Name - HeroService
* import { HeroService } from 'app/sd-services/HeroService';
*/

@Component({
    selector: 'bh-register',
    templateUrl: './register.template.html'
})

export class registerComponent extends NBaseComponent implements OnInit {
    mm: ModelMethods;
    registerform: FormGroup;
    username: any;
    lastname: any;
    userdata:any;
    users: any;
    showmsg:boolean;
    constructor(private bdms: NDataModelService, private registerservice: register) {
        super();
        this.mm = new ModelMethods(bdms);
    }

    ngOnInit() {
        this.getuser();
    }

    displayedColumns: string[] = ['name','lastname'];
    async submit() {
        var data = {
            username: this.username,
            lastname: this.lastname
        }
        this.userdata = (await this.registerservice.register(data)).local.result;
        console.log(this.userdata.result.ok);
        if(this.userdata.result.ok === 1) {
            this.showmsg = true;
            this.getuser();
        }
    }
   async getuser() {
       var status = ( await this.registerservice.getusers()).local.result;
       console.log('response',status);
       this.users= this.convertdata(status);
        console.log('array',this.users);
    }

    convertdata(obj) {
        return Array.from(Object.keys(obj), k => obj[k]);
    }
}
